const express = require('express');
const bodyParser = require('body-parser');
const Boom = require('@hapi/boom');
const logger = require('./logger');

const errorHandler = require('./error-handler');

process.on('uncaughtException', error => {
  logger.error(error, 'uncaughtException');
  process.exit(1);
});

process.on('unhandledRejection', error => {
  throw error;
});

function service(configure) {
  const app = express();
  app.use(bodyParser.json());

  if (configure) {
    configure(app);
  }

  app.use(req => {
    throw Boom.notFound(`Handler for ${req.method} ${req.url} not found.`);
  });

  app.use(errorHandler);

  return app;
}

module.exports = service;
