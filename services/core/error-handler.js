const logger = require('./logger');
const Boom = require('@hapi/boom');

module.exports = function(err, req, res, next) {
  if (!err) {
    return next(err);
  }

  if (err.isAxiosError) {
    if (err.code === 'ECONNABORTED') {
      res.status(408).send({
        statusCode: 408,
        message: 'Timed Out',
        details: 'Request Time-out'
      });
    }

    if (!err.response) {
      logger.error(err, 'Unexpected Error');
      res.status(500).send({
        statusCode: 500,
        message: 'Unexpected Error',
        details: 'Unexpected Error'
      });
    }
    return res.status(err.response.status).send(err.response.data);
  }

  if (err.isJoi) {
    return res.status(400).send({
      statusCode: 400,
      message: err.message,
      details: err.details
    });
  }

  if (Boom.isBoom(err)) {
    return res.status(err.output.statusCode).send({
      statusCode: err.output.statusCode,
      message: err.output.payload.error,
      details: err.output.payload.message
    });
  }

  if (err.name == 'JsonWebTokenError') {
    return res.status(400).send({
      statusCode: 400,
      message: 'Invalid Token',
      details: err.message
    });
  }

  logger.error(err, 'Unexpected Error');
  res.status(500).send({
    statusCode: 500,
    message: 'Unexpected Error',
    details: 'Unexpected Error'
  });
};
