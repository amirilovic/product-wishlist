const config = require('./config');
const logger = require('@pw/core/logger');
const dbClient = require('./repositories/client');
const http = require('http');
const createSocketServer = require('./socketServer');
const app = require('./server');
const listener = require('./listener');
const productMessageHandler = require('./product-message-handler');

dbClient.connect().then(() => {
  logger.info(`Connected to mongodb...`);
});

const server = http.createServer(app);

server.listen(config.app.port, () => {
  logger.info(
    `Server ${config.app.name} started on port ${config.app.port} ...`
  );
});

const socketServer = createSocketServer(server);

listener(productMessageHandler(socketServer));
