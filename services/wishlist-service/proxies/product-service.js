const axios = require('axios');
const { productSvcHost } = require('../config');
const FormData = require('form-data');

const instance = axios.create({
  baseURL: `${productSvcHost}/api`,
  timeout: 5000
});

module.exports = {
  getProductById: async id => (await instance.get(`/products/${id}`)).data,
  getProducts: async ({ limit, offset }) =>
    (await instance.get(`/products?limit=${limit}&offset=${offset}`)).data,
  addProduct: async data => (await instance.post('/products', data)).data,
  updateProduct: async (id, data) =>
    (await instance.put(`/products/${id}`, data)).data,
  deleteProduct: async id => (await instance.delete(`/products/${id}`)).data,
  addProductImage: async (id, file) => {
    const formData = new FormData();
    formData.append('file', file);
    return (await instance.post(`/products/${id}/images`, formData, {
      headers: {
        'Content-Type': `multipart/form-data; boundary=${formData._boundary}`
      }
    })).data;
  }
};
