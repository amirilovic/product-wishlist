const logger = require('@pw/core/logger');

const AWS = require('aws-sdk');
const config = require('./config');

const sqs = new AWS.SQS({
  credentials: config.aws.credentials,
  ...config.aws.sqs
});

const params = {
  QueueUrl: config.aws.sqs.queueUrl,
  MaxNumberOfMessages: 1,
  VisibilityTimeout: 0,
  WaitTimeSeconds: 0
};

const receiveMessages = callback => {
  if (!callback) {
    return;
  }
  sqs.receiveMessage(params, (err, data) => {
    if (err) {
      logger.error(err, 'Error receiving message.');
    } else if (data.Messages) {
      const message = JSON.parse(data.Messages[0].Body);

      callback(message);

      const deleteParams = {
        QueueUrl: config.aws.sqs.queueUrl,
        ReceiptHandle: data.Messages[0].ReceiptHandle
      };
      sqs.deleteMessage(deleteParams, err => {
        if (err) {
          logger.error(err, 'Error receiving message.');
          return;
        }
      });
    }

    setTimeout(() => receiveMessages(callback), 1000);
  });
};

module.exports = receiveMessages;
