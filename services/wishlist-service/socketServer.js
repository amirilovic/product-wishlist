const SocketIO = require('socket.io');
const verify = require('./jwt-verifier');
const db = require('./repositories');
const Boom = require('@hapi/boom');

module.exports = server => {
  const io = SocketIO(server);
  io.use(async (socket, next) => {
    try {
      if (!socket.handshake.query || !socket.handshake.query.token) {
        throw Boom.unauthorized();
      }

      const payload = await verify(socket.handshake.query.token);
      // eslint-disable-next-line require-atomic-updates
      socket.userId = payload.id;
    } catch (err) {
      next(err);
    }
  }).on('connection', async socket => {
    const wishlists = await db.wishlists.getByUserId(socket.userId);

    const wishlistIds = wishlists.map(wishlist => wishlist._id);
    socket.join(wishlistIds);
  });

  return {
    sendForWishlists: (wishlists, { type, payload }) => {
      wishlists.forEach(wishlist => {
        io.in(wishlist._id).emit(type, payload);
      });
    }
  };
};
