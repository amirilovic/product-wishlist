const db = require('../../../repositories');
const server = require('../../../server');
const request = require('supertest');
const nock = require('nock');
const config = require('../../../config');
const Boom = require('@hapi/boom');

jest.mock('../../../repositories');

const token =
  'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkN2Q0OGIyOTRmODMyZTA3ZTFlYzI4MyIsImlhdCI6MTU2ODQ5MjYyMH0.MPpXI8sMqVefC_ZRPet23fQqcj41l1vXWWonyKFx3JrNHBcif2PbHqPlVunPgj3IlKttbfpZ7Mh8JI2GAzBgKvxvqR1b46Js4BXkToAgzTfA2tHHBvAAw9k2qD5MVR9qYP1yg42BvhpzX4zgP0xRryfCZT0tuh3xl4LOIplBeCiFXTdYST1NouY7ivJdpdUggDUs0roaYm9hr6fu0u0tYyO87zx04hCcgzZcREgvwXbFvJ4pNO_aKOFnHl8QualKq9JyHgf1rrgVFI_42hAhDrfM1NiE2aXOveXQbqHAFvL4_rk2YB-X53P9nlCUJM6w0SuUg0qd6ZCfpy_Zr2HHtw';

describe('POST /api/wishlists/:id/products', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('adds product to wishlist', async () => {
    const wishlist = {
      name: 'test',
      userId: '5d7d48b294f832e07e1ec283',
      createdAt: '2019-10-19T10:30:51.525Z',
      _id: '5daae5dbc5264f67d3f5b6fa'
    };

    const product = {
      id: 1,
      name: 'Test',
      price: 11,
      categoryId: 1,
      category: {
        id: 1,
        name: 'Shoes'
      }
    };

    const user = {
      _id: '5d7d48b294f832e07e1ec283',
      username: 'test@test.com',
      gender: 'M',
      createdAt: '2019-10-18T18:57:26.590Z'
    };

    const requestData = {
      productId: product.id
    };

    db.wishlists.getById
      .mockResolvedValueOnce({
        ...wishlist
      })
      .mockResolvedValueOnce({
        ...wishlist,
        productIds: [product.id]
      });

    nock(config.userSvcHost)
      .get(`/api/users/${user._id}`)
      .reply(200, {
        ...user
      });

    nock(config.productSvcHost)
      .get(`/api/products/${product.id}`)
      .times(2)
      .reply(200, {
        ...product
      });

    const response = await request(server)
      .post(`/api/wishlists/${wishlist._id}/products`)
      .set('Authorization', token)
      .send(requestData);

    expect(response.status).toBe(200);
    expect(response.body.name).toBe(wishlist.name);
    expect(response.body.userId).toBe(wishlist.userId);
    expect(response.body.createdAt).toBe(wishlist.createdAt);
    expect(response.body._id).toBe(wishlist._id);
    expect(response.body.products.length).toBe(1);
    expect(response.body.products[0]).toEqual(product);
  });

  it('validates data', async () => {
    const requestData = {};

    const response = await request(server)
      .post(`/api/wishlists/5d7d48b294f832e07e1ec283/products`)
      .set('Authorization', token)
      .send(requestData);

    expect(response.status).toBe(400);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "details": Array [
          Object {
            "context": Object {
              "key": "productId",
              "label": "productId",
            },
            "message": "\\"productId\\" is required",
            "path": Array [
              "productId",
            ],
            "type": "any.required",
          },
        ],
        "message": "{
        \\"userId\\": \\"5d7d48b294f832e07e1ec283\\",
        \\"wishlistId\\": \\"5d7d48b294f832e07e1ec283\\",
        [41m\\"productId\\"[0m[31m [1]: -- missing --[0m
      }
      [31m
      [1] \\"productId\\" is required[0m",
        "statusCode": 400,
      }
    `);
  });

  it('validates auth token', async () => {
    const requestData = {
      productId: 1
    };

    const response = await request(server)
      .post(`/api/wishlists/5d7d48b294f832e07e1ec283/products`)
      .send(requestData);

    expect(response.status).toBe(401);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "details": "Unauthorized",
        "message": "Unauthorized",
        "statusCode": 401,
      }
    `);
  });

  it('validates that wishlist exists', async () => {
    const wishlistId = '5daae5dbc5264f67d3f5b6fa';

    const product = {
      id: 1,
      name: 'Test',
      price: 11,
      categoryId: 1,
      category: {
        id: 1,
        name: 'Shoes'
      }
    };

    const user = {
      _id: '5d7d48b294f832e07e1ec283',
      username: 'test@test.com',
      gender: 'M',
      createdAt: '2019-10-18T18:57:26.590Z'
    };

    const requestData = {
      productId: product.id
    };

    db.wishlists.getById.mockRejectedValue(Boom.notFound());

    nock(config.userSvcHost)
      .get(`/api/users/${user._id}`)
      .reply(200, {
        ...user
      });

    nock(config.productSvcHost)
      .get(`/api/products/${product.id}`)
      .reply(200, {
        ...product
      });

    const response = await request(server)
      .post(`/api/wishlists/${wishlistId}/products`)
      .set('Authorization', token)
      .send(requestData);

    expect(response.status).toBe(404);
    expect(response.body.message).toMatchInlineSnapshot(`"Not Found"`);
  });

  it('validates that user is owner', async () => {
    const wishlist = {
      name: 'test',
      userId: 'fake-id',
      createdAt: '2019-10-19T10:30:51.525Z',
      _id: '5daae5dbc5264f67d3f5b6fa'
    };

    const product = {
      id: 1,
      name: 'Test',
      price: 11,
      categoryId: 1,
      category: {
        id: 1,
        name: 'Shoes'
      }
    };

    const user = {
      _id: '5d7d48b294f832e07e1ec283',
      username: 'test@test.com',
      gender: 'M',
      createdAt: '2019-10-18T18:57:26.590Z'
    };

    const requestData = {
      productId: product.id
    };

    db.wishlists.getById.mockResolvedValue({
      ...wishlist
    });

    nock(config.userSvcHost)
      .get(`/api/users/${user._id}`)
      .reply(200, { ...user });

    nock(config.productSvcHost)
      .get(`/api/products/${product.id}`)
      .reply(200, {
        ...product
      });

    const response = await request(server)
      .post(`/api/wishlists/${wishlist._id}/products`)
      .set('Authorization', token)
      .send(requestData);

    expect(response.status).toBe(403);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "details": "User is not the owner of specified wishlist.",
        "message": "Forbidden",
        "statusCode": 403,
      }
    `);
  });

  it('validates that user exists', async () => {
    const wishlist = {
      name: 'test',
      userId: 'fake-id',
      createdAt: '2019-10-19T10:30:51.525Z',
      _id: '5daae5dbc5264f67d3f5b6fa'
    };

    const product = {
      id: 1,
      name: 'Test',
      price: 11,
      categoryId: 1,
      category: {
        id: 1,
        name: 'Shoes'
      }
    };

    const user = {
      _id: '5d7d48b294f832e07e1ec283',
      username: 'test@test.com',
      gender: 'M',
      createdAt: '2019-10-18T18:57:26.590Z'
    };

    const requestData = {
      productId: product.id
    };

    db.wishlists.getById.mockResolvedValue({
      ...wishlist
    });

    nock(config.userSvcHost)
      .get(`/api/users/${user._id}`)
      .reply(404, { message: 'Not Found' });

    nock(config.productSvcHost)
      .get(`/api/products/${product.id}`)
      .reply(200, {
        ...product
      });

    const response = await request(server)
      .post(`/api/wishlists/${wishlist._id}/products`)
      .set('Authorization', token)
      .send(requestData);

    expect(response.status).toBe(404);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "message": "Not Found",
      }
    `);
  });

  it('validates that product exists', async () => {
    const wishlist = {
      name: 'test',
      userId: '5d7d48b294f832e07e1ec283',
      createdAt: '2019-10-19T10:30:51.525Z',
      _id: '5daae5dbc5264f67d3f5b6fa'
    };

    const product = {
      id: 1,
      name: 'Test',
      price: 11,
      categoryId: 1,
      category: {
        id: 1,
        name: 'Shoes'
      }
    };

    const user = {
      _id: '5d7d48b294f832e07e1ec283',
      username: 'test@test.com',
      gender: 'M',
      createdAt: '2019-10-18T18:57:26.590Z'
    };

    const requestData = {
      productId: product.id
    };

    db.wishlists.getById.mockResolvedValue({
      ...wishlist
    });

    nock(config.userSvcHost)
      .get(`/api/users/${user._id}`)
      .reply(200, { ...user });

    nock(config.productSvcHost)
      .get(`/api/products/${product.id}`)
      .reply(404, { message: 'Not Found' });

    const response = await request(server)
      .post(`/api/wishlists/${wishlist._id}/products`)
      .set('Authorization', token)
      .send(requestData);

    expect(response.status).toBe(404);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "message": "Not Found",
      }
    `);
  });

  it('validates that product is unique in the list', async () => {
    const product = {
      id: 1,
      name: 'Test',
      price: 11,
      categoryId: 1,
      category: {
        id: 1,
        name: 'Shoes'
      }
    };

    const wishlist = {
      name: 'test',
      userId: '5d7d48b294f832e07e1ec283',
      createdAt: '2019-10-19T10:30:51.525Z',
      _id: '5daae5dbc5264f67d3f5b6fa',
      productIds: [product.id]
    };

    const user = {
      _id: '5d7d48b294f832e07e1ec283',
      username: 'test@test.com',
      gender: 'M',
      createdAt: '2019-10-18T18:57:26.590Z'
    };

    const requestData = {
      productId: product.id
    };

    db.wishlists.getById.mockResolvedValue({
      ...wishlist
    });

    nock(config.userSvcHost)
      .get(`/api/users/${user._id}`)
      .reply(200, { ...user });

    nock(config.productSvcHost)
      .get(`/api/products/${product.id}`)
      .reply(200, { ...product });

    const response = await request(server)
      .post(`/api/wishlists/${wishlist._id}/products`)
      .set('Authorization', token)
      .send(requestData);

    expect(response.status).toBe(400);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "details": "Product can only be added once to wishlist",
        "message": "Bad Request",
        "statusCode": 400,
      }
    `);
  });
});
