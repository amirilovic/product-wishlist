const db = require('../../../repositories');
const server = require('../../../server');
const request = require('supertest');

jest.mock('../../../repositories');

const token =
  'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkN2Q0OGIyOTRmODMyZTA3ZTFlYzI4MyIsImlhdCI6MTU2ODQ5MjYyMH0.MPpXI8sMqVefC_ZRPet23fQqcj41l1vXWWonyKFx3JrNHBcif2PbHqPlVunPgj3IlKttbfpZ7Mh8JI2GAzBgKvxvqR1b46Js4BXkToAgzTfA2tHHBvAAw9k2qD5MVR9qYP1yg42BvhpzX4zgP0xRryfCZT0tuh3xl4LOIplBeCiFXTdYST1NouY7ivJdpdUggDUs0roaYm9hr6fu0u0tYyO87zx04hCcgzZcREgvwXbFvJ4pNO_aKOFnHl8QualKq9JyHgf1rrgVFI_42hAhDrfM1NiE2aXOveXQbqHAFvL4_rk2YB-X53P9nlCUJM6w0SuUg0qd6ZCfpy_Zr2HHtw';

describe('POST /api/me/wishlists', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('creates new wishlist', async () => {
    const wishlist = {
      name: 'test',
      userId: '5d7d48b294f832e07e1ec283',
      createdAt: '2019-10-19T10:30:51.525Z',
      _id: '5daae5dbc5264f67d3f5b6fa'
    };

    const requestData = {
      name: wishlist.name
    };

    db.wishlists.insert.mockResolvedValue({
      ...wishlist
    });

    const response = await request(server)
      .post('/api/me/wishlists')
      .set('Authorization', token)
      .send(requestData);

    expect(response.status).toBe(200);
    expect(response.body.name).toBe(wishlist.name);
    expect(response.body.userId).toBe(wishlist.userId);
    expect(response.body.createdAt).toBe(wishlist.createdAt);
    expect(response.body._id).toBe(wishlist._id);
    expect(response.body.products.length).toBe(0);
  });

  it('validates data', async () => {
    const wishlist = {};

    const response = await request(server)
      .post('/api/me/wishlists')
      .set('Authorization', token)
      .send(wishlist);

    expect(response.status).toBe(400);
    expect(response.body.message).toMatchInlineSnapshot(`
      "{
        [41m\\"name\\"[0m[31m [1]: -- missing --[0m
      }
      [31m
      [1] \\"name\\" is required[0m"
    `);
  });

  it('validates auth token', async () => {
    const wishlist = {
      name: 'test',
      userId: '5d7d48b294f832e07e1ec283',
      createdAt: '2019-10-19T10:30:51.525Z',
      _id: '5daae5dbc5264f67d3f5b6fa'
    };

    const response = await request(server)
      .post('/api/me/wishlists')
      .send(wishlist);

    expect(response.status).toBe(401);
    expect(response.body.message).toMatchInlineSnapshot(`"Unauthorized"`);
  });
});
