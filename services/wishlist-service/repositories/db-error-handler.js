const Boom = require('@hapi/boom');

module.exports = function handleError(err) {
  if (err.code == 11000) {
    throw Boom.badRequest('Record already exists');
  }

  throw err;
};
