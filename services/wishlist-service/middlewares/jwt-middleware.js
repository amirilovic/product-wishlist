const Boom = require('@hapi/boom');
const verify = require('../jwt-verifier');

module.exports = async function(req, res, next) {
  try {
    if (!req.headers.authorization) {
      throw Boom.unauthorized();
    }

    const [, token] = req.headers.authorization.split(' ');
    const payload = await verify(token);
    // eslint-disable-next-line require-atomic-updates
    req.userId = payload.id;
    next();
  } catch (err) {
    next(err);
  }
};
