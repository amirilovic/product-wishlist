const config = require('./config');
const jwt = require('jsonwebtoken');

module.exports = token => {
  return jwt.verify(token, config.jwt.publicKey, { algorithms: ['RS256'] });
};
