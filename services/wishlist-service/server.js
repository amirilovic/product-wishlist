const service = require('@pw/core/service');
const wishlistRoutes = require('./api/wishlist');
const productRoutes = require('./api/product');
const userRoutes = require('./api/user');
const config = require('./config');

const app = service(app => {
  app.get('/', (req, res) => res.send(`${config.app.name} 🚀`));
  app.use('/api', wishlistRoutes);
  app.use('/api', userRoutes);
  app.use('/api', productRoutes);
});

module.exports = app;
