const db = require('../../repositories');
const presentWishlists = require('../wishlist-presenter');

module.exports = async function getWishlists(userId) {
  const wishlists = await db.wishlists.getByUserId(userId);
  return presentWishlists(wishlists);
};
