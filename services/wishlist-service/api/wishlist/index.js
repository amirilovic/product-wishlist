const updateWishlist = require('./update-wishlist');

const getWishlistById = require('./get-wishlist-by-id');

const deleteWishlist = require('./delete-wishlist');

const getCurrentUserWishlists = require('./get-current-user-wishlists');

const addCurrentUserWishlist = require('./add-current-user-wishlist');

const addWishlistProduct = require('./add-wishlist-product');

const { Router } = require('express');
const router = Router();
const asyncHandler = require('express-async-handler');
const auth = require('../../middlewares/jwt-middleware');

router.post(
  '/me/wishlists',
  auth,
  asyncHandler(async (req, res) => {
    res.send(await addCurrentUserWishlist(req.userId, req.body));
  })
);

router.get(
  '/me/wishlists',
  auth,
  asyncHandler(async (req, res) => {
    res.send(await getCurrentUserWishlists(req.userId));
  })
);

router.get(
  '/wishlists/:id',
  auth,
  asyncHandler(async (req, res) => {
    res.status(200).send(
      await getWishlistById({
        userId: req.userId,
        wishlistId: req.params.id
      })
    );
  })
);

router.put(
  '/wishlists/:id',
  auth,
  asyncHandler(async (req, res) => {
    res.status(200).send(
      await updateWishlist({
        userId: req.userId,
        wishlistId: req.params.id,
        ...req.body
      })
    );
  })
);

router.delete(
  '/wishlists/:id',
  auth,
  asyncHandler(async (req, res) => {
    await deleteWishlist({
      userId: req.userId,
      wishlistId: req.params.id
    });
    res.status(204).send();
  })
);

router.post(
  '/wishlists/:id/products',
  auth,
  asyncHandler(async (req, res) => {
    res.send(
      await addWishlistProduct({
        userId: req.userId,
        wishlistId: req.params.id,
        productId: req.body.productId
      })
    );
  })
);

module.exports = router;
