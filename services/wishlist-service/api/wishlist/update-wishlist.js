const db = require('../../repositories');
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const userSvc = require('../../proxies/user-service');
const presentWishlists = require('../wishlist-presenter');

const schema = Joi.object({
  userId: Joi.string().required(),
  wishlistId: Joi.string().required(),
  name: Joi.string().required()
}).required();

module.exports = async function addProduct(data) {
  const { userId, wishlistId, name } = Joi.attempt(data, schema);

  const [user, wishlist] = await Promise.all([
    userSvc.getUserById(userId),
    db.wishlists.getById(wishlistId)
  ]);

  if (wishlist.userId !== user._id) {
    throw Boom.forbidden('User is not the owner of specified wishlist.', {
      userId,
      ownerId: wishlist.userId
    });
  }

  await db.wishlists.update(wishlist._id, { name });

  const updatedWishlist = await db.wishlists.getById(wishlistId);

  return (await presentWishlists([updatedWishlist]))[0];
};
