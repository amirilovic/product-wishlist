const db = require('../../repositories');
const presentWishlists = require('../wishlist-presenter');
const userSvc = require('../../proxies/user-service');
const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');

const schema = Joi.object({
  userId: Joi.string().required(),
  wishlistId: Joi.string().required()
}).required();

module.exports = async function getWishlistById(data) {
  const { userId, wishlistId } = Joi.attempt(data, schema);
  const [user, wishlist] = await Promise.all([
    userSvc.getUserById(userId),
    db.wishlists.getById(wishlistId)
  ]);

  if (wishlist.userId !== user._id) {
    throw Boom.forbidden('User is not the owner of specified wishlist.', {
      userId,
      ownerId: wishlist.userId
    });
  }

  return (await presentWishlists([wishlist]))[0];
};
