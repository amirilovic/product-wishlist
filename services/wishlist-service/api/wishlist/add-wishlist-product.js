const db = require('../../repositories');
const Joi = require('@hapi/joi');
const userSvc = require('../../proxies/user-service');
const productSvc = require('../../proxies/product-service');
const Boom = require('@hapi/boom');
const presentWishlists = require('../wishlist-presenter');

const schema = Joi.object({
  userId: Joi.string().required(),
  productId: Joi.number().required(),
  wishlistId: Joi.string().required()
}).required();

module.exports = async function addWishlistProduct(data) {
  const { userId, productId, wishlistId } = Joi.attempt(data, schema);

  const [user, product, wishlist] = await Promise.all([
    userSvc.getUserById(userId),
    productSvc.getProductById(productId),
    db.wishlists.getById(wishlistId)
  ]);

  if (wishlist.userId !== user._id) {
    throw Boom.forbidden('User is not the owner of specified wishlist.', {
      userId,
      ownerId: wishlist.userId
    });
  }

  if (wishlist.productIds && wishlist.productIds.indexOf(product.id) != -1) {
    throw Boom.badRequest('Product can only be added once to wishlist', {
      productId,
      wishlistId,
      userId
    });
  }

  await db.wishlists.addProduct(wishlist._id, product.id);

  const updatedWishlist = await db.wishlists.getById(wishlist._id);

  return (await presentWishlists([updatedWishlist]))[0];
};
