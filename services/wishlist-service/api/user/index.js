const getUserById = require('./get-user-by-id');

const authUser = require('./auth-user');

const addUser = require('./add-user');

const { Router } = require('express');
const router = Router();
const asyncHandler = require('express-async-handler');
const auth = require('../../middlewares/jwt-middleware');

router.post(
  `/users`,
  asyncHandler(async (req, res) => {
    res.send(await addUser(req.body));
  })
);

router.post(
  `/users/auth`,
  asyncHandler(async (req, res) => {
    res.send(await authUser(req.body));
  })
);

router.get(
  `/users/:id`,
  auth,
  asyncHandler(async (req, res) => {
    res.send(await getUserById(req.params.id));
  })
);

module.exports = router;
