const userSvc = require('../../proxies/user-service');

module.exports = function authUser(data) {
  return userSvc.auth(data);
};
