const userSvc = require('../../proxies/user-service');

module.exports = function addUser(data) {
  return userSvc.addUser(data);
};
