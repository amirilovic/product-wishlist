const productService = require('../../proxies/product-service');

module.exports = async function addProduct(data) {
  return productService.addProduct(data);
};
