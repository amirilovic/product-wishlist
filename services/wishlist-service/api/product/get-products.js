const productService = require('../../proxies/product-service');

module.exports = async function getProducts(query) {
  return productService.getProducts(query);
};
