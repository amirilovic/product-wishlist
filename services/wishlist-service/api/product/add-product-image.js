const productService = require('../../proxies/product-service');

module.exports = async function addProduct(id, file) {
  return productService.addProductImage(id, file);
};
