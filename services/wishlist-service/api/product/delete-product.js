const productService = require('../../proxies/product-service');

module.exports = async function deleteProduct(id) {
  return productService.deleteProduct(id);
};
