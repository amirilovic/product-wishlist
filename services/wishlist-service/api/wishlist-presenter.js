const productSvc = require('../proxies/product-service');
module.exports = async function presentWishlists(wishlists) {
  const productIds = Array.from(
    new Set(wishlists.flatMap(wishist => wishist.productIds || []))
  );

  const products = await Promise.all(
    productIds.map(productId => productSvc.getProductById(productId))
  );

  const productsMap = products.reduce((acc, product) => {
    acc[product.id] = product;
    return acc;
  }, {});

  return wishlists.map(wishlist => {
    return {
      ...wishlist,
      products: (wishlist.productIds || []).map(
        productId => productsMap[productId]
      )
    };
  });
};
