const client = require('./client');
const ObjectId = require('mongodb').ObjectID;
const Boom = require('@hapi/boom');
const handleError = require('./db-error-handler');

module.exports = {
  getById: async id => {
    const db = await client.getDb();
    const collection = db.collection('users');
    const user = await collection.findOne({ _id: new ObjectId(id) });
    if (!user) {
      throw Boom.notFound(`User with id "${id}" not found.`);
    }

    return user;
  },
  insert: async user => {
    try {
      const db = await client.getDb();
      const collection = db.collection('users');
      const result = await collection.insertOne({
        ...user,
        createdAt: new Date()
      });
      return result.ops[0];
    } catch (err) {
      handleError(err);
    }
  },
  getByUsername: async username => {
    const db = await client.getDb();
    const collection = db.collection('users');
    const user = await collection.findOne({ username });

    if (!user) {
      throw Boom.notFound(`User  not found.`);
    }

    return user;
  }
};
