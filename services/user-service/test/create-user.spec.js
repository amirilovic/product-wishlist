const db = require('../repositories');
const server = require('../server');
const request = require('supertest');

jest.mock('../repositories');

describe('POST /api/users', () => {
  it('creates new user', async () => {
    const user = {
      username: 'test-user',
      password: 'passw0rd',
      gender: 'M',
      _id: 'fake-id',
      createdAt: new Date().toISOString()
    };

    const requestData = {
      username: user.username,
      password: user.password,
      gender: user.gender
    };

    db.users.insert.mockResolvedValue({
      ...user
    });

    const response = await request(server)
      .post('/api/users')
      .send(requestData);

    expect(response.status).toBe(200);
    expect(response.body.user.username).toBe(user.username);
    expect(response.body.user.gender).toBe(user.gender);
    expect(response.body.user._id).toBe(user._id);
    expect(response.body.user.createdAt).toBe(user.createdAt);
    expect(response.body.user.password).not.toBeDefined();
    expect(response.body.token).toBeDefined();
  });

  it('validates data', async () => {
    const user = {
      password: 'passw0rd',
      gender: 'M'
    };

    const response = await request(server)
      .post('/api/users')
      .send(user);

    expect(response.status).toBe(400);
    expect(response.body.message).toMatchInlineSnapshot(`
      "{
        \\"password\\": \\"passw0rd\\",
        \\"gender\\": \\"M\\",
        [41m\\"username\\"[0m[31m [1]: -- missing --[0m
      }
      [31m
      [1] \\"username\\" is required[0m"
    `);
  });
});
