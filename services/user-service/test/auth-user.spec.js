const db = require('../repositories');
const server = require('../server');
const request = require('supertest');
const passwordService = require('../password-service');

jest.mock('../repositories');

describe('POST /api/auth', () => {
  it('authenticates user', async () => {
    const password = 'passw0rd';
    const username = 'test-user';
    const user = {
      _id: 'fake-id',
      username,
      password: await passwordService.hash(password),
      gender: 'M',
      createdAt: new Date().toISOString()
    };

    const requestData = {
      username,
      password
    };

    db.users.getByUsername.mockResolvedValue({
      ...user
    });

    const response = await request(server)
      .post('/api/auth')
      .send(requestData);

    expect(response.status).toBe(200);
    expect(response.body.user.username).toBe(user.username);
    expect(response.body.user.gender).toBe(user.gender);
    expect(response.body.user._id).toBe(user._id);
    expect(response.body.user.createdAt).toBe(user.createdAt);
    expect(response.body.user.password).not.toBeDefined();
    expect(response.body.token).toBeDefined();
  });

  it('validates data', async () => {
    const user = {
      password: 'passw0rd'
    };

    const response = await request(server)
      .post('/api/users')
      .send(user);

    expect(response.status).toBe(400);
    expect(response.body.message).toMatchInlineSnapshot(`
      "{
        \\"password\\": \\"passw0rd\\",
        [41m\\"username\\"[0m[31m [1]: -- missing --[0m
      }
      [31m
      [1] \\"username\\" is required[0m"
    `);
  });

  it('returns error if passwords do not match', async () => {
    const password = 'passw0rd';
    const username = 'test-user';
    const user = {
      _id: 'fake-id',
      username,
      password: await passwordService.hash(password),
      gender: 'M',
      createdAt: new Date().toISOString()
    };

    const requestData = {
      username,
      password: password + '1'
    };

    db.users.getByUsername.mockResolvedValue({
      ...user
    });

    const response = await request(server)
      .post('/api/auth')
      .send(requestData);

    expect(response.status).toBe(404);
    expect(response.body.message).toMatchInlineSnapshot(`"Not Found"`);
  });
});
