require('dotenv').config();

module.exports = {
  app: {
    name: process.env.APP_NAME,
    port: process.env.PORT || 3000,
    env: process.env.APP_ENV || 'development'
  },
  mongo: {
    url: process.env.MONGO_URL,
    database: process.env.MONGO_DATABSE
  },
  jwt: {
    passphrase: process.env.JWT_PASSPHRASE,
    privateKey: process.env.JWT_PRIVATE_KEY,
    publicKey: process.env.JWT_PUBLIC_KEY
  }
};
