const server = require('./server');
const config = require('./config');
const logger = require('@pw/core/logger');

server.listen(config.app.port, () => {
  logger.info(
    `Server ${config.app.name} started on port ${config.app.port} ...`
  );
});
