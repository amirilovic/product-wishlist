const logger = require('@pw/core/logger');

const AWS = require('aws-sdk');
const config = require('./config');

const sqs = new AWS.SQS({
  credentials: config.aws.credentials,
  ...config.aws.sqs
});

module.exports = {
  productUpdated: product => {
    return sqs
      .sendMessage({
        QueueUrl: config.aws.sqs.queueUrl,
        MessageBody: JSON.stringify({
          type: 'PRODUCT_UPDATED',
          payload: product
        })
      })
      .promise()
      .catch(error => logger.error(error, 'PRODUCT_UPDATE failed'));
  },
  productCreated: product => {
    return sqs
      .sendMessage({
        QueueUrl: config.aws.sqs.queueUrl,
        MessageBody: JSON.stringify({
          type: 'PRODUCT_CREATED',
          payload: product
        })
      })
      .promise()
      .catch(error => logger.error(error, 'PRODUCT_CREATED failed'));
  },

  productDeleted: product => {
    return sqs
      .sendMessage({
        QueueUrl: config.aws.sqs.queueUrl,
        MessageBody: JSON.stringify({
          type: 'PRODUCT_DELETED',
          payload: product
        })
      })
      .promise()
      .catch(error => logger.error(error, 'PRODUCT_DELETED failed'));
  }
};
