const knex = require('./client');
const Boom = require('@hapi/boom');
const errorHandler = require('./db-error-handler');

const PRODUCTS = 'products';

module.exports = {
  find: ({ offset, limit }) => {
    return knex
      .from(PRODUCTS)
      .select('*')
      .orderBy('id', 'asc')
      .limit(limit)
      .offset(offset);
  },
  findByIds: (ids = []) => {
    return knex
      .from(PRODUCTS)
      .select('*')
      .whereIn('id', ids);
  },

  getById: async id => {
    const product = await knex
      .from(PRODUCTS)
      .where('id', id)
      .first('*');

    if (!product) {
      throw Boom.notFound(`Product with id: ${id} doesn't exist.`);
    }

    return product;
  },

  update: async (id, data) => {
    return knex(PRODUCTS)
      .update(data)
      .where({ id })
      .catch(errorHandler);
  },

  insert: async data => {
    try {
      const [id] = await knex(PRODUCTS).insert(data);
      return id;
    } catch (err) {
      errorHandler(err);
    }
  },

  deleteById: id => {
    return knex(PRODUCTS)
      .where({ id })
      .delete();
  }
};
