const dbErrors = require('db-errors');
const Boom = require('@hapi/boom');

const handleError = err => {
  const dbError = dbErrors.wrapError(err);

  if (dbError instanceof dbErrors.ForeignKeyViolationError) {
    throw Boom.badRequest('Validation Error', {
      table: dbError.table,
      constraint: dbError.constraint
    });
  }

  if (dbError instanceof dbErrors.UniqueViolationError) {
    throw Boom.badRequest('Validation Error', {
      table: dbError.table,
      columns: dbError.columns
    });
  }

  throw err;
};

module.exports = handleError;
