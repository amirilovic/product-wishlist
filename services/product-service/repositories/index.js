module.exports = {
  product: require('./products'),
  productCategories: require('./product-categories'),
  productImages: require('./product-images')
};
