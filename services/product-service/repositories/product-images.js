const knex = require('./client');
const Boom = require('@hapi/boom');
const errorHandler = require('./db-error-handler');

const PRODUCT_IMAGES = 'productImages';

module.exports = {
  findByProductIds: (productIds = []) => {
    return knex
      .from(PRODUCT_IMAGES)
      .select('*')
      .whereIn('productId', productIds);
  },
  insert: async data => {
    try {
      const [id] = await knex(PRODUCT_IMAGES).insert(data);
      return id;
    } catch (err) {
      errorHandler(err);
    }
  },
  getById: async id => {
    const product = await knex
      .from(PRODUCT_IMAGES)
      .first('*')
      .where('id', id);

    if (!product) {
      throw Boom.notFound(`Product with id: ${id} doesn't exist.`);
    }

    return product;
  },
  findByIds: ids => {
    return knex
      .from(PRODUCT_IMAGES)
      .select('*')
      .whereIn('id', ids);
  },
  deleteByProductId: productId => {
    return knex(PRODUCT_IMAGES)
      .delete()
      .where({ productId });
  }
};
