const knex = require('./client');

const PRODUCT_CATEGORIES = 'productCategories';

module.exports = {
  findByIds: (ids = []) => {
    return knex
      .from(PRODUCT_CATEGORIES)
      .select('*')
      .whereIn('id', ids);
  }
};
