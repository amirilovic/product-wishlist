const server = require('../server');
const request = require('supertest');
const dbHelper = require('./db-test-helper')();
jest.mock('../uploader', () => {
  return {
    productImages: {
      upload: () =>
        Promise.resolve({ Location: 'http://www.server.com/random.jpg' })
    }
  };
});

const productCategories = [
  {
    name: 'A'
  },
  {
    name: 'B'
  },
  {
    name: 'C'
  }
];

const products = [
  {
    name: 'Super Product',
    price: 99.99,
    categoryId: 1
  }
];

describe('POST /api/products', () => {
  beforeAll(async () => {
    await dbHelper.setupDb();
    await dbHelper.resetDb();
    await dbHelper.seedDb({ productCategories, products });
  });
  beforeEach(async () => {
    await dbHelper.resetDb(['productImages']);
  });
  afterAll(async () => {
    await dbHelper.dropDb();
  });
  it('creates new product image', async () => {
    const response = await request(server)
      .post('/api/products/1/images')
      .attach('file', __dirname + '/mock-data/stan-smith.jpg');

    expect(response.status).toBe(200);
    expect(response.body[0].id).toBeDefined();
    expect(response.body[0].url).toBeDefined();
    expect(response.body[0].productId).toBe(1);
  });
});
