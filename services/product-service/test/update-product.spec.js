jest.mock('../publisher');
const server = require('../server');
const request = require('supertest');
const dbHelper = require('./db-test-helper')();
const { productUpdated } = require('../publisher');

const productCategories = [
  {
    name: 'A'
  },
  {
    name: 'B'
  },
  {
    name: 'C'
  }
];

describe('PUT /api/products/:id', () => {
  beforeAll(async () => {
    await dbHelper.setupDb();
    await dbHelper.resetDb();
    await dbHelper.seedDb({ productCategories });
  });
  beforeEach(async () => {
    await dbHelper.resetDb(['productImages', 'products']);
  });
  afterAll(async () => {
    await dbHelper.dropDb();
  });
  it('updates existing product', async () => {
    const product = {
      name: 'Super Product',
      price: 99.99,
      categoryId: 1
    };

    await dbHelper.seedDb({ products: [product] });

    const newProductData = {
      name: 'Super Product 1'
    };
    const response = await request(server)
      .put('/api/products/1')
      .send(newProductData);

    expect(response.status).toBe(200);
    expect(response.body.id).toBeDefined();
    expect(response.body.name).toBe(newProductData.name);
    expect(response.body.price).toBe(product.price);
    expect(response.body.categoryId).toBe(product.categoryId);
    expect(response.body.category.name).toBe(productCategories[0].name);
    expect(productUpdated).toHaveBeenCalledTimes(1);
  });

  it('validates request properties', async () => {
    const product = {};
    const response = await request(server)
      .put('/api/products/1')
      .send(product);

    expect(response.status).toBe(400);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "details": Array [
          Object {
            "context": Object {
              "label": "value",
              "peers": Array [
                "name",
                "price",
                "categoryId",
              ],
              "peersWithLabels": Array [
                "name",
                "price",
                "categoryId",
              ],
            },
            "message": "\\"value\\" must contain at least one of [name, price, categoryId]",
            "path": Array [],
            "type": "object.missing",
          },
        ],
        "message": "{
        [41m\\"value\\"[0m[31m [1]: -- missing --[0m
      }
      [31m
      [1] \\"value\\" must contain at least one of [name, price, categoryId][0m",
        "statusCode": 400,
      }
    `);
  });

  it('validates category', async () => {
    const product = {
      name: 'Super Product',
      price: 99.99,
      categoryId: 1
    };

    await dbHelper.seedDb({ products: [product] });

    const newProductData = {
      categoryId: 404
    };

    const response = await request(server)
      .put('/api/products/1')
      .send(newProductData);

    expect(response.status).toBe(400);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "details": "Validation Error",
        "message": "Bad Request",
        "statusCode": 400,
      }
    `);
  });

  it('returns 404 if id does not exist', async () => {
    const newProductData = {
      categoryId: 1
    };

    const response = await request(server)
      .put('/api/products/404')
      .send(newProductData);

    expect(response.status).toBe(404);
    expect(response.body).toMatchInlineSnapshot(`
      Object {
        "details": "Product with id: 404 doesn't exist.",
        "message": "Not Found",
        "statusCode": 404,
      }
    `);
  });
});
