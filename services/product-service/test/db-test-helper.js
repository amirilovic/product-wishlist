const config = require('../config');
const mysql = require('mysql2/promise');
const knex = require('../repositories/client');

module.exports = function() {
  const conn = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password
  });

  const execute = async (...args) => {
    return (await conn).execute(...args);
  };

  const setupDb = async () => {
    await execute(`DROP DATABASE IF EXISTS \`${config.db.name}\``);
    await execute(`CREATE DATABASE \`${config.db.name}\``);
    await knex.migrate.latest();
  };

  const dropDb = async () => {
    await knex.destroy();
    await execute(`DROP DATABASE \`${config.db.name}\``);
    await (await conn).end();
    await (await conn).close();
  };

  const seedDb = async data => {
    for (let table of Object.keys(data)) {
      await knex(table).insert(data[table]);
    }
  };

  const resetDb = async (
    tables = ['productImages', 'products', 'productCategories']
  ) => {
    const trx = await knex.transaction();

    await knex.raw('SET FOREIGN_KEY_CHECKS=0').transacting(trx);

    await Promise.all(
      tables.map(table =>
        knex(table)
          .truncate()
          .transacting(trx)
      )
    );

    await knex.raw('SET FOREIGN_KEY_CHECKS=1').transacting(trx);
    trx.commit();
  };

  return {
    setupDb,
    resetDb,
    dropDb,
    seedDb
  };
};
