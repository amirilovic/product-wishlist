const server = require('../server');
const request = require('supertest');
const dbHelper = require('./db-test-helper')();

const productCategories = [
  {
    name: 'A'
  },
  {
    name: 'B'
  },
  {
    name: 'C'
  }
];

describe('GET /api/products/:id', () => {
  beforeAll(async () => {
    await dbHelper.setupDb();
    await dbHelper.resetDb();
    await dbHelper.seedDb({ productCategories });
  });
  beforeEach(async () => {
    await dbHelper.resetDb(['productImages', 'products']);
  });
  afterAll(async () => {
    await dbHelper.dropDb();
  });
  it('returns product with category', async () => {
    const product = {
      name: 'Super Product',
      price: 99.99,
      categoryId: 1
    };

    await dbHelper.seedDb({ products: [product] });

    const response = await request(server)
      .get('/api/products/1')
      .send(product);

    expect(response.status).toBe(200);
    expect(response.body.id).toBeDefined();
    expect(response.body.name).toBe(product.name);
    expect(response.body.price).toBe(product.price);
    expect(response.body.categoryId).toBe(product.categoryId);
    expect(response.body.category.name).toBe(productCategories[0].name);
  });

  it('returns 404 if id not found', async () => {
    const response = await request(server).get('/api/products/404');

    expect(response.status).toBe(404);
    expect(response.body.message).toMatchInlineSnapshot(`"Not Found"`);
  });
});
