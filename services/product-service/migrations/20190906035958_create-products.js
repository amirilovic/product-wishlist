exports.up = function(knex) {
  return knex.schema.createTable('products', t => {
    t.increments('id');
    t.string('name').notNullable();
    t.decimal('price').notNullable();
    t.integer('categoryId').unsigned();
    t.foreign('categoryId').references('productCategories.id');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('products');
};
