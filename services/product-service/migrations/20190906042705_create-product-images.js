exports.up = function(knex) {
  return knex.schema.createTable('productImages', t => {
    t.increments('id');
    t.string('url').notNullable();
    t.integer('productId').unsigned();
    t.foreign('productId').references('products.id');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('productImages');
};
