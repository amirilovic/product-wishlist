exports.up = function(knex) {
  return knex.schema.createTable('productCategories', t => {
    t.increments('id');
    t.string('name').notNullable();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('productCategories');
};
