const AWS = require('aws-sdk');
const config = require('./config');

const s3 = new AWS.S3({
  credentials: config.aws.credentials,
  ...config.aws.s3,
  s3BucketEndpoint: !!config.aws.s3.endpoint,
  s3ForcePathStyle: true
});

function uploader(bucket) {
  return {
    upload: (path, stream) => {
      return s3
        .upload({
          Bucket: bucket,
          Key: path,
          Body: stream
        })
        .promise();
    },
    remove: path => {
      return s3
        .deleteObject({
          Bucket: bucket,
          Key: path
        })
        .promise();
    }
  };
}

module.exports = {
  productImages: uploader('pw-product-images')
};
