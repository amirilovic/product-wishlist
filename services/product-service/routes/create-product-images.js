const uploader = require('../uploader');

const db = require('../repositories');
const path = require('path');
const uuidv4 = require('uuid/v4');

module.exports = async function createProductImages(id, files) {
  const product = await db.product.getById(id);
  const productImageIds = await Promise.all(
    files.map(async file => {
      const uploadedFile = await uploader.productImages.upload(
        `${product.id}/${uuidv4()}${path.extname(file.filename)}`,
        file
      );
      return db.productImages.insert({
        url: uploadedFile.Location,
        productId: product.id
      });
    })
  );

  const productImages = await db.productImages.findByIds(productImageIds);

  return productImages;
};
