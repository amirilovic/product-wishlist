const db = require('../repositories');
const Joi = require('@hapi/joi');
const presentProducts = require('./products-presenter');

const schema = Joi.number().required();

module.exports = async function getProduct(data) {
  const id = Joi.attempt(data, schema);
  const product = await db.product.getById(id);
  return (await presentProducts([product]))[0];
};
