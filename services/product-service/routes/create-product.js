const publisher = require('../publisher');

const db = require('../repositories');
const Joi = require('@hapi/joi');
const presentProduct = require('./products-presenter');

const schema = Joi.object({
  name: Joi.string().required(),
  price: Joi.number().required(),
  categoryId: Joi.number().required()
}).required();

module.exports = async function createProduct(data) {
  const productData = Joi.attempt(data, schema);
  const id = await db.product.insert(productData);
  const product = await db.product.getById(id);
  const productResponse = (await presentProduct([product]))[0];

  //NOTE: not waiting for message publish to finish
  publisher.productCreated(productResponse);

  return productResponse;
};
