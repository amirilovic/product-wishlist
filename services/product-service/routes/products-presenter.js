const db = require('../repositories');

module.exports = async function presentProduct(products) {
  const productIds = products.map(p => p.id);
  const productCategoryIds = products.map(p => p.categoryId);
  const [productCategories, productImages] = await Promise.all([
    db.productCategories.findByIds(productCategoryIds),
    db.productImages.findByProductIds(productIds)
  ]);

  const productCategoriesIndex = productCategories.reduce((acc, category) => {
    acc[category.id] = category;
    return acc;
  }, {});

  const productImagesIndex = productImages.reduce((acc, image) => {
    if (!acc[image.productId]) {
      acc[image.productId] = [];
    }
    acc[image.productId].push(image);
    return acc;
  }, {});

  return products.map(product => {
    return {
      ...product,
      category: productCategoriesIndex[product.categoryId],
      images: productImagesIndex[product.id]
    };
  });
};
