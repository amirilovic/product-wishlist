const publisher = require('../publisher');

const productsPresenter = require('./products-presenter');

const db = require('../repositories');
const Joi = require('@hapi/joi');

const schema = Joi.object({
  name: Joi.string(),
  price: Joi.number(),
  categoryId: Joi.number()
}).or('name', 'price', 'categoryId');

module.exports = async function updateProduct(id, data) {
  const updateData = Joi.attempt(data, schema);
  const product = await db.product.getById(id);
  await db.product.update(product.id, updateData);
  const productNew = await db.product.getById(product.id);

  const productResponse = (await productsPresenter([productNew]))[0];

  publisher.productUpdated(productResponse);

  return productResponse;
};
