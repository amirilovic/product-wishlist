# NodeJS Workshop

# Demo Application

For the purpose of this workshop we are going to build product wishlist application. Demo simulates real-world microservices architecture by separating functionalities in separate services and showing different communication patterns between them.

On a high level product wishlist application allows users to create an account, login to the system browse list of products and create their wishlists. For this purpose we decided to develop the following services:

## Products Service

Provides CRUD endpoints for products on top of MySQL.
Supports upload of multiple product images and upload to S3
Publishes changes to other services via Message Queue

## User Service

Provides user login and register endpoints and issuing JWT.
Stores data in MongoDB.

## Wishlist Service

Exposes login and register endpoints
Exposes products read endpoints
Provides CRUD endpoints for wishlists with JWT validation.
Provides CRUD endpoints for wishlist items with JWT validation.
Subscribes to product updates queue and publishes updates to users via socket.io

![Product Wishlist Application](./docs/demo-app.png)

Detalied API with list of all endpoints and constraints is described in [swagger.yaml](./swagger.yaml) or you can view it at: https://app.swaggerhub.com/apis-docs/amirilovic/product-wishlist/1.0.0

Workshop will start like real-word projects by creating one monolith projects and then work on separating products and users services.
