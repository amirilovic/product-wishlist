# Building First Service

## Overview

We are going to start build the whole solution as single service, then we are going to evolve it to extract some functionality into separate services, but we are not going to break API interface to our clients.

## Web Framework Alternatives

### Express

Fast, unopinionated, minimalist web framework for node. https://expressjs.com (45 276 ⭐)

### HapiJS

🏢 Server Framework for Node.js https://hapi.dev (11 409 ⭐️)

### Fastify

Fast and low overhead web framework, for Node.js https://www.fastify.io (11 791 ⭐️)

### NestJS

A progressive Node.js framework for building efficient, scalable, and enterprise-grade server-side applications on top of TypeScript & JavaScript (ES6, ES7, ES8) 🚀 https://nestjs.com/ (18 999 ⭐️)

## Directory structure

All backend services will be created in `services` folder. Each services will contain:

- `server.js` configures web server with all routes and middlewares.
- `index.js` starts the server on a port.
- `config.js` creates service configuration.
- `api` contains all api routes.
- `repositories` contains all repositories which execute db operations.
- `migrations` contains all db migration files.
- `test` contains all integration tests.

## Create first route

- Create a route `POST /products`
- Use `body-parser` to get body payload
- Validate payload
  - TIP: use `@hapi/joi`

## Restart server when files changes

- Add nodemon as **development** dependency
- Create `start:dev` task that will restart server using nodemon when any file in product-service changes
- Make sure you can debug your app with vscode when running in dev mode
  - TIP: add `--inspect` flag

## App logger

- Use `pino`
- Use `pino-pretty` for development
  - TIP: just pipe output to `pino-pretty`

## App configuration

- Use `dotenv`
- Create `.env` and add to `.gitignore`
- Create `.env.example` as a template for development setup and copy `.env`
- Create `config.js` that uses `dotenv`
- Add name of the service to config
- Add service port to the config

## Error handling

- What types of errors can we get?
  - Validaton errors
  - DB errors
    - TIP: MySQL errors reference: https://dev.mysql.com/doc/refman/5.5/en/server-error-reference.html
  - Unexpected errors
- Use `next` callback to handle all errors
- Write global error handler that handles all error types
  - TIP: use `db-errors` to handle database errors.
  - TIP: make sure you log all unexpected errors.
  - TIP: make sure that you send sensitive info only in development.
- Handle express route not found error
- Handle `uncaughtException`, `unhandledRejection`

## Using promises

- Use `express-async-handler` and return promises instead of working with callbacks

## Practice

- Create endpoint `GET /api/products/:id`
- Create endpoint `PUT /api/products/:id`
- Create endpoint `DELETE /api/products/:id`
- Create endpoint `GET /api/products?limit=:limit&offset=:offset`
