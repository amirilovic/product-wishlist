# Working with Users

## Working with JWT (jsonwebtoken)

- What is JWT?

![JWT auth](./jwt-auth-flow.png)

- How to issue token?
- How to validate token?
  - Create express middleware for auth, handle auth errors

## Create migration to store users

- name: bigint, autoinc
- username: decimal, required, unique
- password: string, required, encrypted
- isAdmin: bool, required, default: false
- gender: string
- createdAt: dateTime, required

## Create new user -> returns created user.

- Handle `POST /users` request
- Validate payload data

  - name: string, required
  - username: decimal, required, min: 5, max: 15
  - password: string, required, min: 5, max: 15
  - gender: string

- Encrypt password
  - TIP: use `bcrypt` package `hash` function to encrypt passwords
- Add `createdAt` info
- Insert data in `users` collection
- Handle unique index error
- Create json web token (JWT) with user id

  - TIP: If one service is issuing tokens and others are verifing them, make sure to use public and private key pair to sign and verify tokens, don't use just secret - never share secrets with other services, that will enable other services to also issue tokens.
  - Generate rsa key pair

  ```bash
  $ openssl genrsa -des3 -out private.pem 2048
  $ openssl rsa -in private.pem -outform PEM -pubout -out public.pem
  ```

  - use `jsonwebtoken` package to sign and verify JWT

- Return newly created user with JWT and **without password**
- Create `user-presenter`

## Practice

- Create login endpoint `POST /api/users/auth`
  - TIP: use `bcrypt` package `compare` function to verify password
- Create get user by id `POST /api/users/:id`
- Protect product endpoints that mutate data
