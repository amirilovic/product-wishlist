# Integration Testing

- Setup test runner (https://jestjs.io/docs/en/getting-started)
- Mock db repositories
- Supertest
- Test product create endpoint
- Next to the file you want to test create `.spec.js` file and start writing tests.

## Practice

Test following endopints

- `GET /api/products/:id`
- `PUT /api/products/:id`
- `DELETE /api/products/:id`
- `GET /api/products?limit=:limit&offset=:offset`
