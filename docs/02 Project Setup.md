# Project Setup

## Prerequsities

Make sure to create GitLab and Heroku accounts.

You will need following software to follow along:

- git
- docker
- Postman
- nvm
- node 12
- Heroku CLI
- VSCode with extensions
  - prettier
  - eslint

## Seed project

Fork the seed project at: https://gitlab.com/amirilovic/nodejs-seed to your GitLab account and clone it.

For project has following tools already configured:

- nvm (.nvm) - node version manager (https://github.com/nvm-sh/nvm)
- editorconfig (.editorconfig) - whitespace configuration for IDE (https://editorconfig.org/)
- prettier (.prettierrc, .prettierignore) - code formatting rules (https://prettier.io/)
- eslint (.eslintrc.js, .eslintignore) - code quality rules (https://eslint.org/)
- jest - test runner (https://jestjs.io/)

Check node version:

```bash
$ node --version
```

If you don't have node 12, make sure you install it:

```bash
$ nvm install 12
```

Now any time you are in the project in terminal you can do:

```bash
$ nvm use
```

This will set the correct version of node.
