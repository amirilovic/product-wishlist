# Realtime Features

## Setup SQS queue

Take a look at `scripts/localstack/init.sh`:

```bash
export SQS_QUEUE=pw-products
export AWS_REGION=us-east-1

awslocal sqs create-queue --queue-name $SQS_QUEUE --region $AWS_REGION
```

- Publishing messages in `product-service` when product is created
- Receive and handle product created messages from `wishlist-service`
- Broadcasting updates to clients using `socket.io`

## Practice

Publish messages about:

- product updates
- product delete
